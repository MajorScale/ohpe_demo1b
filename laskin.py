result_number = 0

while True:
    print(f"Luku on {result_number}.")
    operation = input("Anna operaatio (tyhjä lopettaa): ")

    if operation == "":
        print("Kiitos ja moi!")
        break

    op = operation[0] # get operator
    val = operation[1:]
    if op == "+":
        result_number += float(val)
    elif op == "-":
        result_number -= float(val)
    elif op == "*":
        result_number *= float(val)
    elif op == "/":
        result_number /= float(val)
    

